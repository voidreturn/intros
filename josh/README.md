I'm voidreturn.

I build software sometimes.

## Technology

Software is built by loading an understanding of technology and using the understanding to build a system that can be used by a human; or a computer.

This gives us new hypothetical understanding, like the internet.

The internet is tcp/ip and dns binding human readable addresses to machine addresses.

Within the internet there are protocols for doing things.

HTTP communicates on port 80 and lets us send data to each other.

HTTPS communicates on port 443 and lets us send data that is encrypted.

HTML is a markup language that is sent using the above protocols; it lets us send webpages as messages.

Javascript is a Turing complete system that allows us to build logical applications on top of webpages.

Email uses the SMTP (Simple Mail Transfer Protocol) to facilitate one to one, short term, instant communication of data. This data can be in the form of a webpage.

Google is a search tool for finding http(s) dns addresses with webpages that have content that relates to a human supplied query. The ability to search the internet.

Amazon EC2 is a software interface for creating and destroying virtual machines running on physical hardware. This is monetized by owning computational power and leasing it in the form of virtual machines.

Facebook is a combination of proprietary systems allowing one to many, long term, creation and communication of digital identity. It creates long term, many to many, communication of identity.

Note that previously, this form of human communication did not exist in our reality.

Reality did not have one to many, or many to many instant communication; much less many to many long term communication of psychological understanding.

50 years ago no one gave a shit about remembering what they had for breakfast, or where they had it because they never saw a picture of john having avocado toast at the bistro on 4th on their timeline; because john could never have communicated to 5 people, let alone 500 people, that he had avocado toast at the bistro on 4th 43 hours ago.

No one gave a shit because there was no way to document that understanding. Psychological energy was never consumed documenting and later understanding this type of human knowledge.

Modern technology increases the total supply of human knowledge in existence. It gives alternative outlets for psychological energy that did not previously exist.

We (humans) haven't been very good at directing what psychological energy systems do or do not get built.

Capitalism has - for the most part - dictated what systems are built.

People need to have the ability to dictate what psychological energy systems are built, before they're built.

Facebook became successful because people used it. People using something is not an indication that the thing being used is good. It simply means money can be made.

_In context psychological energy can be considered synonymous with [libidinal energy](https://en.wikipedia.org/wiki/Libido#Analytical_psychology)._

## Side Effects

Companies like Facebook and Amazon didn't exist before.

There was never a lumberjack pimping wood at 5000 locations; with 2 million people cutting down trees and selling logs 150 years ago.

It didn't exist because it was impossible for a human to directly communicate with 2 million people in their lifetime until relatively recently.

Technology has gone from a shovel giving leverage in moving objects, to controlled burning of fuel to move metal parts connecting a seat to tires, to silicon chips with more than a billion transistor mechanisms logically connected into Turing complete systems; in a square inch.

Once Turing completeness emerges in a reality there is a fundamental change in _what_ technology is. It unlocks a new scale of psychological energy systems.

Pre-turing psychological energy systems:
  - religions
  - governments
  - businesses

Pre-turing systems should probably be updated; our reality might get [filtered](https://en.wikipedia.org/wiki/Great_Filter) 😅.

## Moving Forward

Netflix has an interesting thing of one to many, long term, communication, using art.

I think it would be good to have one to many, _short term_, communication, using art.

Netflix subsidizes the conversion of synthetic understanding from artists into video and sound data.

Netflix works because they produce things that people want to watch. The content is mostly passive communication. There is no intrinsic need to remember the data. This gives the user the luxury of consuming content without worrying about information or value gained.

Learning is an active task, and takes energy. There is a need to remember the data, or understanding communicated by the data. This is probably best done in short intervals, with quizzes to test understanding and possibly a reward for successful learning.

Financially rewarding users for learning using a token would probably be a good implementation of universal basic income.

**Remember**: Universal basic income is a type of system, it can be implemented by anyone that has the means; financial or otherwise.

## But what have _you_ been doing, voidreturn, you loudmouthed cunt

In the past ~50 hours I built a token distribution system.

It creates tokens in an Ethereum smart contract and runs daily dutch auctions selling a fixed number of tokens for ether. The entity creating the contract receives control of the ether; tokens are distributed over a fixed amount of time. Unbought tokens are destroyed, reducing the total supply (increasing the value of existing tokens).

I've been considering running a distribution for 1 year to build a system creating a many to many, anonymous, long term, communication system with tokens as a financial mechanic to incentivize real world action.

I believe the above combination of technology can be used to produce an effective unionization system.

I believe an effective unionization system may help balance the existence of government, companies and people.

Government and companies both have power, now people need it.

### Incentives and money

I believe most software companies to be structurally inefficient in terms of psychology.

Raising money from an investor to build a product for a user creates opposing incentives. The aligned incentive is the users desire to use the platform; use of a platform is not an indication that the platform is good, only that it can likely make money (the incentive of the investor). The actual aligned incentive should likely be a real world gain, like education or human rights.

Some of the most conceptually valuable companies have this flaw: software that allows you to produce short term transportation in less than 5 minutes is inherently powerful. Fund it with investor money and the goal is no longer to build the best transportation system; it's to build the most profitable transportation system.

### Cryptocurrencies

Creating a company funded by selling tokens that will be used in a psychological energy system seems to avoid the opposing incentives of above. Instead the money is collected once, in the sale of the token to fund the creation of the system.

Selling tokens over the time it will take to develop the system seems like a clean way to bind software companies into capitalism.

Selling the token over time at a fixed rate prevents individuals from purchasing huge amounts early, and thus gaining disproportionate amounts of value.

It helps reduce the advantage of knowledge in time; selling the entirety of a token supply at once heavily advantages those that actively research cryptocurrencies. Distributing at a fixed rate over time helps equalize the buying opportunity, regardless of when knowledge of the token is gained.

Selling the entirety of the token and burning any unbought tokens ensures that raised ether is put back into the system when the company needs tokens to use the system.

The seller of the token will have to use ether to purchase tokens; otherwise they will have no tokens.

The token becomes an immutable financial automaton.

## Why are you telling me this

I seem to think you have quite a good idea.

Creating a communication system for short, directed learning in the form of tasks to be completed offers a sort of determinism that can't be achieved with content on a webpage.

It is monitoring the expenditure of time on the completion of a task. In so doing it gives the task an upfront time price, offering the user the cost benefit analysis of knowledge to be gained vs time spent.

It distributes the knowledge in time; and offers concrete tasks to achieve goals.

Producing and curating short, educational videos will likely be successful. Alternating those with quizzes at a point in the future would likely be successful as well.

Netflix produces entertainment content by paying artists.

Perhaps in the case of learning content it should be produced by paying educators.

I believe schools do the same thing.

You'll probably need an engineer.
